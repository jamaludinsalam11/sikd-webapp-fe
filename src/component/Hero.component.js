import React, {useEffect, useState} from 'react';
import { Alert, Row, Col } from 'react-bootstrap';
const Hero = () => {
    const [opacitys, setOpactity] = useState(1);

    useEffect(() => {

        const handleScroll = () => {
          if(window.scroll){
            setOpactity( 1 - window.scrollY / 700);
          }
          
        }
    
        window.addEventListener('scroll', handleScroll);
        
        return () => window.removeEventListener('scroll', handleScroll)
    
      }, [opacitys])
    

    return (
        <div className="hero sticky" style={{opacity: opacitys}}>
            <section className="banner-id">
                <img  className="hero-bg" src="./assets/banner.svg" />
                    <div className="banner">
                        <div className="container banner-scroll-hide pt-5">
                            <Row>
                            <Col className="banner-left">
                                <h2 className="animate__animated animate__backInLeft">SIKD</h2>
                                <p className="animate__animated animate__backInUp">Sistem Informasi Kearsipan Dinamis</p>
                            </Col>
                            <Col className="banner-right">
                                <img  className="ban-img image-fluid " src="./assets/hero-image.svg" />
                            </Col>
                                
                            </Row>
                        </div>
                    </div>
            </section>
        </div>
    )
}

export default Hero;