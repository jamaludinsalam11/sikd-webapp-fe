import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Results from './Results.component';
import SkeletonResults from './SkeletonResults.component';

const IndexContent = () => {
    const [sikd, setSikd] = useState({ data: [], isFetching: false });
    const [search, setSearch] = useState('');
    const [query, setQuery] = useState('359.150121101957')

    
    const handleSearch = event => {
        setSearch(event.target.value);
        // console.log(search)
    }

    const handleQuery = event => {
        if(search === null){
            setQuery('string');
        } else{
            setQuery(search);

        }
        console.log(query)
    }

    useEffect(() => {
        const fetchSikd = async () => {
            try{
                setSikd({data: sikd.data, isFetching: true});
                const response = await axios.get(`https://sikd.jamsdev.work/api/sikd2?NId=${query}`);
                var datafetch = response.data.data;

                const unique = [];
                datafetch.map(x => unique.filter(a => a.GIR_Id == x.GIR_Id).length > 0 ? null : unique.push(x));
                setSikd({data: unique, isFetching: false})
            } catch (e){
                console.log(e);
                setSikd({sikd: sikd.data, isFetching: false});
            }
        }
        fetchSikd()
    },[query])

    const sikdfetch = sikd.data;
    console.log(sikdfetch)
    console.log(search)

    return(
        <section id="content">
            <div className="container-fluid">
                    <div className="card shadow">
                            <div className="search">
                                    <div className="form-row pb-3">
                                        <input 
                                            className="form-control form-control-lg" 
                                            type="text" 
                                            placeholder="Search"
                                            value={search}
                                            onChange={handleSearch}
                                            
                                            
                                        />
                                    </div>
                                    <div className="form-row justify-content-center">
                                        <button 
                                            type="submit" 
                                            onClick={handleQuery}
                                            className="btn btn-primary btn-search">
                                            
                                            Search
                                        </button>
                                    </div>
                            </div>
                            {sikdfetch.length === null ? 'isNull' : <Results data={sikdfetch}/>}
                            
                    </div>
            </div>
        </section>
    )
}


export default IndexContent;