import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
const SubRow = ({datasub}) => {
    const [receiver, setReceiver] = useState({data: {}, isFetching: false});

    useEffect(() => {
        const fetchReceiver = async () => {
            try{
                setReceiver({data: receiver.data, isFetching: true})
                const response = await axios.get('http://95.111.198.177:3005/penerima?n='+ datasub.NId + '&g=' + datasub.GIR_Id);
                console.log(response.data.penerima);
                setReceiver({data: response.data.penerima, isFetching:false})
            } catch(err){
                console.log(err)
            }
        }
        fetchReceiver()
    }, [])



    const fetchData = receiver.data.penerima;
    console.log(fetchData)

    return (
        <>
        {fetchData.map((item, key) => (
        <TableRow key={key}>
            <TableCell component="th" scope="row">
                
            </TableCell>
            <TableCell></TableCell>
            <TableCell ></TableCell>
            <TableCell >
            </TableCell>
        </TableRow>
        ))}
        </>
        
    )
}


export default SubRow;