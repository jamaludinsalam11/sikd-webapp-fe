import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Container } from '@material-ui/core';

const SkeletonResults = () => {
    return (
        <Container maxWidth="lg">
             <TableContainer >
                <Table aria-label="collapsible table">
                    <TableHead>
                        <TableRow>
                        <TableCell />
                        <TableCell>Tanggal</TableCell>
                        <TableCell >Asal Naskah</TableCell>
                        <TableCell >Jabatan</TableCell>
                        <TableCell >Keterangan</TableCell>
                        <TableCell>Pesan</TableCell>
                        <TableCell>File</TableCell>
                        </TableRow>
                    </TableHead>
                </Table>
                <TableBody>

                </TableBody>
             </TableContainer>
        </Container>
    )
}

export default SkeletonResults;