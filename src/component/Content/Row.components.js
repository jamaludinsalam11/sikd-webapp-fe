import React, {useState, useEffect} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import { Container } from '@material-ui/core';
import Chip from '@material-ui/core/Chip';

import SubRow from './SubRow.component';



const useRowStyles = makeStyles({
    root: {
      '& > *': {
        borderBottom: 'unset',
      },
    },
  });

const Row = ({data}) => {
    // const { row } = props;
    const classes = useRowStyles();
    const [open, setOpen] = React.useState(false);
    const [receiver, setReceiver] = useState({data: {}, isFetching: false});

    useEffect(() => {
        const fetchReceiver = async () => {
            try{
                setReceiver({data: receiver.data, isFetching: true})
                const response = await axios.get(`https://sikd.jamsdev.work/api/penerima?n=${data.NId}&g=${data.GIR_Id}`);
                console.log(response.data.penerima);
                setReceiver({data: response.data, isFetching:false})
            } catch(err){
                console.log(err)
            }
        }
        fetchReceiver()
    }, [])



    const tes = receiver.data.penerima;
    console.log(data)
    return (
    <React.Fragment>
        <TableRow className={classes.root}>
            <TableCell>
                <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                    {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                </IconButton>
            </TableCell>
            <TableCell component="th" scope="row">
            {data && data.Inbox.NTglReg === null ? '':data.Inbox.NTglReg}
            </TableCell>
            <TableCell align="left">{data.People.pengirim}</TableCell>
            <TableCell align="left">{data.People.PeoplePosition}</TableCell>
            <TableCell align="left">
            {
                data.Inbox.SifatId === 'XxJyPn38Yh.1' ? 'Disposisi (Biasa)' : 
                data.Inbox.SifatId === 'XxJyPn38Yh.2' ? 'Disposisi (Rahasia Terbatas)':
                data.Inbox.SifatId === 'XxJyPn38Yh.3' ? 'Disposisi (Rahasia)' : 'Disposisi (Sangat Rahasia)'

            }
            </TableCell>
            <TableCell align="left">{data.Inbox.Hal}</TableCell>
            <TableCell className="table-cell" align="center">
                <IconButton aria-label="delete">
                    <CloudDownloadIcon fontSize="large" />
                    
                </IconButton>
                <Typography variant="caption" display="block" gutterBottom>
                    {data.InboxDs.file_ds}
                    </Typography>
            </TableCell>
            
        </TableRow>
        <TableRow>
            <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <Box margin={1}>
                <Typography variant="h6" gutterBottom component="div">
                    Tujuan Naskah
                </Typography>
                <Table size="small" aria-label="purchases">
                    <TableHead>
                    <TableRow>
                        <TableCell>Terkirim</TableCell>
                        <TableCell>Tujuan</TableCell>
                        <TableCell >Status</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {tes && tes.map((items, key) => (
                        <TableRow key={key} >
                            <TableCell component="th" scope="row">
                                {items.Inbox.NTglReg}
                            </TableCell>
                            <TableCell>
                                {items.To_Id_Desc}
                            </TableCell>
                            <TableCell>
                                {items.StatusReceive === 'read' ? (<Chip label="Read" color="primary" />) : <Chip label="Unread" color="secondary" />}
                            </TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </Box>
            </Collapse>
            </TableCell>
        </TableRow>
    </React.Fragment>
    )
}

export default Row;