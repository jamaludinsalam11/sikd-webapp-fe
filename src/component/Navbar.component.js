import React from 'react';


const Navbar = () => {
    return (
        <section className="nav">
            <nav className="shadow-lg logo-navbar sh-navbar  ">
                <div>
                    <a className="navbar-brand center" href="#">
                    <img className="animate__animated animate__pulse animate__infinite animate__slow" src="./assets/img/navbar/GARUDA_white.png" width="100" height="100" alt=""/>
                    </a>
                </div>
            </nav>
        </section>
    )
}

export default Navbar;