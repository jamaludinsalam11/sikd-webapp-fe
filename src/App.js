import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
// import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { Alert, Row, Col } from 'react-bootstrap';
import Navbar from './component/Navbar.component';
import Hero from './component/Hero.component';
import Content from './component/Content/Index.component';


function App() {

  return (
    <>
    <Navbar/>
    <Hero/>
    <Content />

  </>
  );
}

export default App;
